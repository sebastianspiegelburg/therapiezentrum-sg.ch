/**
 * Created by baschi on 21.07.17.
 */



var containerId;
function initaliseLanguage() {
    containerId = "pageArticles";
    changeLanguage("de");
    writeLanguageInNav();
    writeArticleContactInformation();
    initaliseMap();
    writeArticlePhilosophy("WeDo");
    writeArticleThearapie();
    writeArticleWhoAreWe();
    console.log("Executed");

};

function writeLanguageInNav() { 
    $("#idNavButtonContact").text(navButtonContact);
    $("#idNavButtonTherapie").text(navButtonTherapie);
	$("#idNavButtonWeAre").text(navButtonWeAre);
    $("#idNavButtonConcept").text(navButtonConcept);
}

function writeArticlePhilosophy(secKey) {
    $("#pageArticles").append(
        "<div id='concept' class='container-fluid'> \
           <div class='row'><h1 id='idSection" + secKey + "Title'> " + window["Section" + secKey + "Title"] + "</h1></div> \
           <div class='row'> \
                <section id='idSection" + secKey + "' class='col-sm-12'> \
                     <h1><small id='idSection" + secKey + "Subtitle'>" + window["Section" + secKey + "Subtitle"] + "</small></h1> \
                     <p id='idSection" + secKey + "Content'>" + window["Section" + secKey + "Content"] + " </p>\
               </section>\
           </div> \
        </div> \
    ");
}


function writeArticleThearapie() {
    $("#" + containerId).append("<div id='therapie' class='container-fluid'>" +
        "<div class='row'> <h1>" + SectionThearapieTitle + "</h1></div>" +
        "<div class='row'>" +
        "   <section class='col-md-6 col-sm-12'><h1><small>" + SectionThearapie1Subtitle +"</small></h1>" +
        "       <ul id='speechThearapyList'></ul>" +
        "   </section>" +
        "   <section class='col-md-6 col-sm-12'><h1><small>" + SectionThearapie2Subtitle +"</small></h1>" +
        "       <ul id='physicalThearapyList'></ul>" +
        "   </section>" +
        "</div>" +
        "</div>");
    for( _i in SectionThearpieListLogo) {
        $("#speechThearapyList").append("<li>"+ SectionThearpieListLogo[_i] +"</li>");
    }
    $("#speechThearapyList").append("<li><a href='http://therapiezentrum-sg.ch/downloads/LogopaedieVerordnung.pdf'>Logopädie Verordnung</a></li>");
    for( _i in SectionThearpieListPhysothearapie) {
        $("#physicalThearapyList").append("<li>"+ SectionThearpieListPhysothearapie[_i] +"</li>");
    }
    $("#physicalThearapyList").append("<li><a href='http://therapiezentrum-sg.ch/downloads/PhysiotherapieVerordnung.pdf'>Physiotherapie Verordnung</a></li>"); }
function writeArticleExplain(){
    $("#" + containerId).append("<div class='container-fluid'>" +
        "<div class='row'> <h1>" + SectionExplainationTitleMain +"</h1></div>" +
        "<div class='row'>" +
        "   <section class='col-md-6 col-sm-12'><h2><small>" +SectionExplainationSubsectionTitleSpeechThearpy +"</small></h2>" +
        "<p> " + SectionExplainationContentSpeechThearpy +"</p>" +
        "   </section>" +
        "   <section class='col-md-6 col-sm-12'><h2><small>" +SectionExplainationSubsectionTitlePhysicalThearpy +"</small></h2>" +
        "<p> " + SectionExplainationContentPhysicalThearpy +"</p>" +
        "   </section>" +
        "</div>" +
        "</div>");
    }

//<!-- <div class='col-md-4 '><img class='img-responsive logoSize' src='/image/logo.jpeg'></div>-->" +
//belongs in section class row
function writeArticleWhoAreWe() {
    $("#" + containerId).append(
        "<div id='weare' class='container-fluid'>" +
        "   <div class='row'> <h1>" + SectionWhoWeAreTitle  +"</h1></div>" +
        "       <div class='col-sm-12 col-md-6'>" +
        "          <section class='row' >" +
        "                   <div class='col-md-3 '><h1><small>Stephanie Rabiega:</small></h1></div>" +
        "                   <div class='col-md-9 '><ul id='qualificationListLogo'></ul>" +
        "          </section>" +
        "       </div>"   +
        "       <div class='col-sm-12 col-md-6'>" +
        "          <section class='row' >" +
        "                   <div class='col-md-3 '><h1><small>Patryk Rabiega:</small></h1></div>" +
        "                   <div class='col-md-9 '><ul id='qualificationListPhysio'></ul> </div>" +
        "          </section>" +
        "       </div>"   +
        "</div>");
    for( _i in SectionWhoWePhycicalPerson) {
        $("#qualificationListPhysio").append("<li>"+ SectionWhoWePhycicalPerson[_i] +"</li>");
    }
    for( _i in SectionWhoWeSpeechThearapyPerson) {
        $("#qualificationListLogo").append("<li>"+ SectionWhoWeSpeechThearapyPerson[_i] +"</li>");
    }

}


function writeArticleContactInformation() {
    $("#" + containerId).append(
        "<div id='contact' class='container-fluid'>" +
        "   <div class='row'> <h1>" + SubSectionWhereWeAreTitle   +"</h1></div>" +
        "   <div class='row'>"    +
        "       <section class='col-sm-12 col-md-4'>" +
        "           <h1>" +
        "               <small>" + SubSectionWhereWeAreCompanyNamePart1 + "<br></small>" +
        "           </h1>" +
        "               <p>" +
        "                   <span class='glyphicon glyphicon-home contact-spacer' aria-hidden='true'></span>" +
        "                  " + SubSectionWhereWeAreStreetName + "<br>" +
        "                   <span class='contact-spacer-below glyphicon glyphicon-home contact-spacer'></span>" +
        "                  " + SubSectionWhereWeArePC +
        "                  " + SubSectionWhereWeAreCity + "<br>" +
        "                   <span class='glyphicon glyphicon-earphone contact-spacer ' aria-hidden='true'></span>" +
        "                  " + SubSectionWhereWeArePhoneNumber + "<br>" +
        "                   <span class='glyphicon glyphicon-envelope contact-spacer' aria-hidden='true'></span>" +
        "                  " + SubSectionWhereWeAreEmail +
        "               </p>" +
        "       </section>" +
        "       <section class='col-sm-12 col-md-4'>" +
        "           <h1>" +
        "               <small>" + SubSectionOpeningTimesTitel + "<br></small>" +
        "           </h1>" +
        "               <p>" +
        "                   <span class='glyphicon glyphicon-calendar contact-spacer' aria-hidden='true'></span>" +
        "                  " + SubSectionOpeningTimes1 + "<br>" +
        "                   <span class='contact-spacer-below glyphicon glyphicon-home contact-spacer'></span>" +
        "                  " + SubSecT2  + "<br>" +
        "                   <span class='contact-spacer-below glyphicon glyphicon-home contact-spacer'></span>" +
        "                  " + SubSecT3  + "<br>" +
        "                   <span class='contact-spacer-below glyphicon glyphicon-home contact-spacer'></span>" +
        "				   " + SubSectionOpeningTimes2 +
        "               </p>" +
        "          </section>" +
        "       <section class='col-sm-12 col-md-4'>" +
        "           <h1>" +
        "               <small>" + SubSectionRecognisionTitel + "<br></small>" +
        "           </h1>" +
        "           <p>" + SubSectionRecognisionContent + "</p>" +
        "       </section>" +
        "       </div>" +
        "</div>");
}

function initaliseMap(){
	 $("#pageArticles").append("<div id='mapid'></div>");
	var mymap = L.map('mapid').setView([ 47.41553, 9.36038 ], 13);
	L.tileLayer(
					'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}',
					{
						attribution : 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
						maxZoom : 18,
						id : 'mapbox.streets',
						accessToken : 'pk.eyJ1IjoiaWthcnVzMTMiLCJhIjoiY2pkMGNjbTR2NDR6ZDJxbjBjYjB6NmF0MCJ9.19uYGZMmoo9XfsIBO98SVQ'
					}).addTo(mymap);
	
	var marker = L.marker([47.41553, 9.36038 ]).addTo(mymap);
	//marker.bindPopup("<p>Wir freuen uns auf Sie</p>").openPopup();
	}

function initaliseFooder(){}

function changeLanguage(lang) {

    if (lang === "en") {
        var navButtonContact = "Contact";
        var navButtonPlace = "Place";
        var navButtonConcept = "Success Concept";
        var navButtonWeDo = "What we do";

    }
    else {
        var navButtonContact = "Kontakt";
        var navButtonPlace = "Ort";
        var navButtonConcept = "Ein neuer Weg";
        var navButtonWeDo = "Was wir machen";
    }

}

