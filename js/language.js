/**
 * Created by baschi on 19.07.17.
 */


var navButtonContact = "Kontakt";
var navButtonTherapie = "Therapie";
var navButtonConcept = "Ein neuer Weg";
var navButtonWeAre = "Wer wir sind";


var SectionWeDoTitle = "Einen neuen Weg beschreiten - Schritt für Schritt";
var SectionWeDoSubtitle = "";

var SectionWeDoContent =
    "Manchmal nimmt das Leben eine andere Richtung als geplant und wir brauchen \
Zeit, Kraft und die Unterstützung kompetenter Menschen, die uns Schritt für \
 Schritt auf unserem neuen Weg begleiten. Wir vom Therapiezentrum „Schritt für Schritt“ \
 sind spezialisiert auf die Rehabilitation nach neurologischen Ereignissen. Wir unterstützen \
 Sie dabei in der Physiotherapie und in der Logopädie. Unser Ziel \
 ist es, mit Freude, Menschlichkeit und hohem Engagement qualitativ hochwertige Therapien \
 anzubieten. Jeder ist bei uns herzlich willkommen!";




var SectionThearapieTitle = "Therapie";
var SectionThearapie1Subtitle = "In der Logopädie behandeln wir:";
var SectionThearapie2Subtitle = "In der Physiotherapie bieten wir:";

var SectionThearpieListPhysothearapie = [
    "Aktive Bewegungstherapie", "Beckenbodentraining", "Bobath",
    "Entspannungstherapie", "Manuelle Therapie", "Medizinische Trainingstherapie",	"Massagetherapie",
	"Klassische Massage", "Lymphdrainage", "Physikalische Entstauungstherapie", "Therapie auf Polnisch möglich" ]
var SectionThearpieListLogo = [
    "Aphasie (Sprachstörung)", "Dysarthrophonie (Sprechstörung)", "Dysphagie (Schluckstörung)",
    "Fazialisparese (Schwäche oder Lähmung der Gesichtsmuskulatur)", "Dysphonie (Stimmstörungen)",
    "Therapie auf Italienisch möglich"];




var SectionExplainationTitleMain = "Was machen Therapeuten";
var SectionExplainationSubtitleMain = "Rehabilitation";
var SectionExplainationSubsectionTitleSpeechThearpy = "Logopädie ist ..."
var SectionExplainationSubsectionTitlePhysicalThearpy = "Physiotherapie ist ..."
var SectionExplainationContentSpeechThearpy =
    "Die Logopädie beschäftigt sich mit der Kommunikation, der mimischen Muskulatur " +
    "und dem Schlucken. Durch eine Hirnverletzung kann es zu Störungen in diesen " +
    "drei Bereichen kommen, die für den Betroffenen und auch für sein soziales Umfeld zu " +
    "Problemen führen kann. Die Logopädin diagnostiziert Sprach- und Sprechstörungen, " +
    "Schluckstörungen und Gesichtslähmungen. Sie plant gemeinsam mit dem Patienten und seinen " +
    "Angehörigen die Therapieziele und erstellt die therapeutischen Maßnahmen.";

var SectionExplainationContentPhysicalThearpy =
    "Die Physiotherapie ist ein Heilmittel für Patienten jeder Altersstufe, " +
    "das sich in der Gesamtheit mit der Effektivität von Bewegungen und Körperfunktionen beschäftigt. " +
    "Diese Therapie kann vorbeugend, bei akuten Beschwerden oder als rehabilitative Maßnahme " +
    "z.B. zur Wiedereingliederung in Beruf, Alltag, Hobbys oder in das soziale Umfeld eingesetzt werden. " +
    "Nach einer genauen Untersuchung werden mit dem Patienten Ziele für die Therapie festgelegt. " +
    "Während der Therapie werden der Befund und die Behandlung fortlaufend neu evaluiert und " +
    "es wird ein individueller, zielorientierter Behandlungsplan erstellt. " ;




var SectionWhoWeAreTitle = "Wer sind wir";

var SectionWhoWePhycicalPerson = [
	"4-jährige Ausbildung zum staatlich anerkannten Physiotherapeuten in Karlsruhe und Baden-Baden (2004 – 2008)",
	"2-jährige Arbeit in den Klinken Schmieder Stuttgart im neurologischen Rehabilitationskrankenhaus",
	"2-jährige selbständige Tätigkeit in Baden–Baden ",
	"Seit 2013 angestellt im ambulanten Therapiezentrum Rehaclinic Winterthur"];
var SectionWhoWeSpeechThearapyPerson = [
	"3-jährige Ausbildung zur Logopädin an der SHLR (2008 – 2011)",
	"3- jährige Arbeit im ambulanten Therapiezentrum Rehaclinic Winterthur",
	"Stellvertreterin in den Spitälern KSW, Waidspital Zürich, Geriatrische \
	Klinik St.Gallen, Logopädisches Therapiezentrum Rellstab – Rothlin",
	"seit Januar 2017 angestellt in der neurologischen Rehaklink Zihlschlacht"];




var SubSectionWhereWeAreTitle = "Kontakt";
var SubSectionWhereWeAreCompanyNamePart1 = "Therapiezentrum";
var SubSectionWhereWeAreCompanyNamePart2 = "Schritt für Schritt";
var SubSectionWhereWeAreStreetName ="Vonwilstrasse 3";
var SubSectionWhereWeArePC = "9000";
var SubSectionWhereWeAreCity  = "St.Gallen";
var SubSectionWhereWeArePhoneNumber = "078 740 94 90";
var SubSectionWhereWeAreEmail  = "info@therapiezentrum-sg.ch";

var SubSectionOpeningTimesTitel ="Öffnungszeiten";
var SubSectionOpeningTimes1 = "Montag bis Freitag:"
var SubSecT2 =	"8:00-12:00Uhr";
var SubSecT3 =  "14:00-20:00Uhr";
var SubSectionOpeningTimes2 = "Samstag: 8:00-14:00Uhr";
var SubSectionRecognisionTitel = "Krankenkassen anerkannt";
var SubSectionRecognisionContent = "Das " + SubSectionWhereWeAreCompanyNamePart1 + " "  +
    SubSectionWhereWeAreCompanyNamePart2 + " ist von " +
    "allen Krankenkassen und Unfallversicherern, sowie von der IV anerkannt.";



/*
-navbutton Verordnungen
-pdf Verordnungen einbinden unter navbutton mit name: Verordnungen sprung zu : in der Logopädie behandeln wir
-pdf zwei links in jede Kategorie einer




-Raus nehmen: was machen Therapeuten
-Verknüpfung Therapieliste mit Therapieerklärung
- Reihenfolge zuerst Physiotherapie dann Logopädie

-Buttonname "unser Erfolgskonzept" umbenenne zu Schritt für Schri
*/ 

 
 
 
 
/* Hei Baschi Zeilenumbruch nach medizinische Trainingstherapie und nach Physikalische Entstauungstherapie
und unten nach Dysphonie*/